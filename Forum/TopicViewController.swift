//
//  TopicViewController.swift
//  Forum
//
//  Created by Admin on 28/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import UIKit
import RxSwift

class TopicViewController: UIViewController, ViewHelper {
  
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var titleText: UITextField!
  @IBOutlet weak var contentText: UITextView!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  
  var topicVM    = TopicVM()
  var disposeBag = DisposeBag()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.title = "New topic"
    self.contentText.layer.borderWidth = 0.5
    self.contentText.layer.borderColor = UIColor.gray.cgColor
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TopicViewController.dismissKeyboard))
    view.addGestureRecognizer(tap)
   
  }
  
  //Calls this function when the tap is recognized.
  func dismissKeyboard() {
    view.endEditing(true)
  }
  
  @IBAction func savePressed(_ sender: UIButton) {
    guard !(titleText.text?.isEmpty)! else {
      showAlert(message: "Title can't be blank.")
      return
    }
    guard !(contentText.text?.isEmpty)! else {
      showAlert(message: "Content can't be blank")
      return
    }
    self.spinner.startAnimating()
    topicVM.topic.title   = titleText.text!
    topicVM.topic.content = contentText.text!
    topicVM.topic.user_id = User.sharedInstance.id 
    let _ = topicVM.rxCreate()
      .observeOn(MainScheduler.instance)
      .subscribe{result in
        let status = result.element
        guard status == 200 else {
          self.spinner.stopAnimating()
          self.showAlert(message: self.topicVM.errorMessage)
          return
        }
        self.spinner.stopAnimating()
        _ = self.navigationController?.popViewController(animated: true)
    }
      .disposed(by: disposeBag)
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}

extension TopicViewController: UITextFieldDelegate, UITextViewDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    self.view.endEditing(true)
    return true
  }
 
}
