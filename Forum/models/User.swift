//
//  User.swift
//  Forum
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import Foundation

struct User{
  var id       = 0
  var name     = ""
  var password = ""
  var email    = ""
  
  static let sharedInstance = User()
  
  fileprivate init(){
    let defaults = UserDefaults()
    if let nameStr = defaults.string(forKey: "USER_NAME") {
      self.id    = defaults.integer(forKey: "USER_ID")
      self.name  = nameStr
      self.email = defaults.string(forKey: "USER_EMAIL")!
    }else {
      id       = 0
      email    = ""
      name     = ""
      password = ""
    }
  }
  
  func saveUserInDefaults(){
    let defaults = UserDefaults()
    defaults.set(id, forKey: "USER_ID")
    defaults.set(name, forKey: "USER_NAME")
    defaults.set(email, forKey: "USER_EMAIL")
  }
}
