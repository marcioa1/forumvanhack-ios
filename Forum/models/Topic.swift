//
//  Topic.swift
//  Forum
//
//  Created by Admin on 28/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import Foundation

struct Topic{
  var id      = 0
  var title   = ""
  var content = ""
  var user_id = 0
  var posts   = [Post]()
  var date    = Date()
}
