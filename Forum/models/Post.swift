//
//  Post.swift
//  Forum
//
//  Created by Admin on 29/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import Foundation

struct Post{
  var comment  = ""
  var user     = ""
  var date     = Date()
  var topic_id = 0
}
