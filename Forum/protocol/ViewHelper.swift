//
//  ViewHelper.swift
//  RobertosClock
//
//  Created by Admin on 02/09/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import Foundation
import UIKit

protocol ViewHelper{
  func showAlert(message: String)
}

extension ViewHelper{
  func showAlert(message: String) {
    let alertController = UIAlertController(title: "Ops", message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
    alertController.addAction(okAction)
    
    let pushedViewControllers = (AppDelegate.getAppDelegate().window?.rootViewController as!UINavigationController).viewControllers
    let presentedViewController = pushedViewControllers[(pushedViewControllers.count) - 1]
    
    presentedViewController.present(alertController, animated: true, completion: nil)
  }
}
