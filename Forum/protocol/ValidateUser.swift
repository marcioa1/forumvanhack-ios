//
//  ValidateUser.swift
//  Forum
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import Foundation


protocol ValidateUser{
  func userValid(name: String, email: String, password: String) -> String
}

extension ValidateUser{
  func userValid(name: String, email: String, password: String) -> String {
    var message = ""
    var errors = [String]()
    if name.isEmpty {
      errors.append("You must type your name")
    }
    if email.isEmpty {
      errors.append("email can't be blank")
    }
    if password.isEmpty {
      errors.append("password required")
    }
    if password.count < 8 {
      errors.append("password must have at leat 8 characters")
    }
    
    message = errors.joined(separator: ", ")
    return message
  }
}
