//
//  LoginViewController.swift
//  Forum
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import UIKit
import RxSwift

class LoginViewController: UIViewController, ViewHelper {
  
  @IBOutlet weak var emailText: UITextField!
  @IBOutlet weak var passwordText: UITextField!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  
  var loginVM = LoginVM()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.title = "Login"
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func loginButtonPressed(_ sender: UIButton) {
    self.view.endEditing(true)
    guard !(emailText.text?.isEmpty)! else {
      showAlert(message: "Email can't be blank")
      return
    }
    guard !(passwordText.text?.isEmpty)! else {
      showAlert(message: "Passoword can't be blank")
      return
    }
    guard (passwordText.text?.count)! >= 8 else {
      showAlert(message: "Password must have at leat 8 characters")
      return
    }
    spinner.startAnimating()
    loginVM.email    = emailText.text!
    loginVM.password = passwordText.text!
    
    let _ = loginVM.rxLogin()
      .observeOn(MainScheduler.instance)
      .subscribe{result in
        self.spinner.stopAnimating()
        let status = result.element
        if status == 401 {
          self.showAlert(message: "Verify password and try again")
          return
        }
        if status == 404 {
          self.showAlert(message: "User not found")
          return
        }
        if status == 200 {
          self.performSegue(withIdentifier: "open_topics", sender: self)
        }
    }
  }
  
}

extension LoginViewController : UITextFieldDelegate{
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}
