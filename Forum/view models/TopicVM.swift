//
//  TopicVM.swift
//  Forum
//
//  Created by Admin on 28/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class TopicVM {
  
  var topics = [Topic]()
  var topic  = Topic()
  var errorMessage = ""
  
  
  
  func rxIndex() -> Observable<Int> {
    topics.removeAll()
    return Observable.create{ observer in
      let url = Platform.isSimulator ? Url.LOCAL_TOPICS : Url.SERVER_TOPICS
      print(url)
      Alamofire.request(url).responseJSON { response in
        guard response.response?.statusCode == 200 else {
          observer.onNext(404)
          return
        }
        
        guard let JSON = response.result.value as? Array<Any> else {
          observer.onError(NSError(domain: "Dados do servidor inválidos", code: (response.response?.statusCode)!, userInfo: nil))
          return
        }
        
        for cell in JSON {
          let element = cell as! Dictionary<String, Any>
          print(element)
          var topic         = Topic()
          topic.id          = element["id"] as! Int
          topic.title       = element["title"] as! String
          topic.content     = element["content"] as! String
          let posts         = element["posts"] as! Array<NSDictionary>
          for elem in posts {
            var post = Post()
            post.comment = elem["comment"] as! String
            post.user    = elem["user"] as! String

            let dateStr  = elem["created_at"] as! String
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            post.date = dateFormatter.date(from: dateStr)!
          
            topic.posts.append(post)
          }
          self.topics.append(topic)
        }
        observer.onNext(200)
      }
      return Disposables.create{}
    }
    
  }
  
  func rxCreate() -> Observable<Int> {
    return Observable.create{ observer in
      let params = ["topic":["title": self.topic.title, "content": self.topic.content, "user_id": User.sharedInstance.id]]
      
      let url = Platform.isSimulator ? Url.LOCAL_TOPICS : Url.SERVER_TOPICS
      Alamofire.request(url, method: .post, parameters: params).responseJSON { response in
        guard response.response?.statusCode == 200 else {
          guard let responseJSON = response.result.value else {
            self.errorMessage = "Unidentified error."
            observer.onNext(404)
            return
          }
          self.errorMessage  = responseJSON as! String
          observer.onNext(404)
          return
        }
        observer.onNext(200)
      }
      return Disposables.create{}
    }
    
  }
}
