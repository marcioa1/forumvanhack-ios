//
//  LoginVM.swift
//  Forum
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class LoginVM {
  
  var id       = 0
  var name     = ""
  var email    = ""
  var password = ""
  
  func rxLogin() -> Observable<Int> {
    return Observable.create{ observer in
      let params = ["email": self.email, "password": self.password]
      
      let url = Platform.isSimulator ? Url.LOCAL_LOGIN : Url.SERVER_LOGIN
      Alamofire.request(url, parameters: params).responseJSON { response in
        guard response.response?.statusCode != nil else {
          observer.onNext((response.response?.statusCode)!)
          return
        }
        guard response.response?.statusCode == 200 else {
          observer.onNext((response.response?.statusCode)!)
          return
        }
        guard let responseJSON = response.result.value else {
          observer.onNext(404)
          return
        }
        if let dic = responseJSON as? Dictionary<String, Any>{
          self.name  = dic["name"] as! String
          self.id = dic["id"] as! Int
        }
        self.saveUserInDefaults()
        observer.onNext(200)
      }
      return Disposables.create{}
    }
    
  }
  
  func saveUserInDefaults(){
    let defaults = UserDefaults()
    defaults.set(id, forKey: "USER_ID")
    defaults.set(name, forKey: "USER_NAME")
    defaults.set(email, forKey: "USER_EMAIL")
  }
}
