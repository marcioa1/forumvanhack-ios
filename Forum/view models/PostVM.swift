//
//  PostVM.swift
//  Forum
//
//  Created by Admin on 29/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class PostVM {
  
  var post = Post()
  var errorMessage = ""
  
  func rxCreate() -> Observable<Int> {
    return Observable.create{ observer in
      let params = ["post":["comment": self.post.comment,  "user_id": User.sharedInstance.id, "topic_id": self.post.topic_id]]
      
      var url = Platform.isSimulator ? Url.LOCAL_POSTS : Url.SERVER_POSTS
      url = url.replacingOccurrences(of: "topic_id", with: String(self.post.topic_id))
      Alamofire.request(url, method: .post, parameters: params).responseJSON { response in
        guard response.response?.statusCode == 200 else {
          guard let responseJSON = response.result.value else {
            self.errorMessage = "Unidentified error."
            observer.onNext(404)
            return
          }
          self.errorMessage  = responseJSON as! String
          observer.onNext(404)
          return
        }
        observer.onNext(200)
      }
      return Disposables.create{}
    }
    
  }
}
