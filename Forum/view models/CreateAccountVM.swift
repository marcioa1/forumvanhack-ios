//
//  CreateAccountVM.swift
//  Forum
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class CreateAccountVM {
  
  var id       = 0
  var email    = ""
  var name     = ""
  var password = ""
  var errorMessage = ""
  
  func rxCreate() -> Observable<Int> {
    return Observable.create{ observer in
      let params = ["user":["name": self.name, "email": self.email, "password": self.password]]
      
      let url = Platform.isSimulator ? Url.LOCAL_CREATE_ACCOUNT : Url.SERVER_CREATE_ACCOUNT
      Alamofire.request(url, method: .post, parameters: params).responseJSON { response in
        guard response.response?.statusCode == 200 else {
          self.errorMessage = "Unidentified error."
          observer.onNext(404)
          return
        }
        guard let responseJSON = response.result.value else {
          self.errorMessage = "Unidentified error."
          observer.onNext(404)
          return
        }
        let idStr = responseJSON as! String
        self.id   = Int(idStr)!
        self.saveUserInDefaults()
        observer.onNext(200)
      }
      return Disposables.create{}
    }
  }

  func saveUserInDefaults(){
    let defaults = UserDefaults()
    defaults.set(id, forKey: "USER_ID")
    defaults.set(name, forKey: "USER_NAME")
    defaults.set(email, forKey: "USER_EMAIL")
  }
}
