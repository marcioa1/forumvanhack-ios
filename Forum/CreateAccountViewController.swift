//
//  CreateAccountViewController.swift
//  Forum
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import UIKit
import RxSwift

class CreateAccountViewController: UIViewController, ValidateUser, ViewHelper {

  @IBOutlet weak var nameText: UITextField!
  @IBOutlet weak var emailText: UITextField!
  @IBOutlet weak var passwordText: UITextField!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  
  var createAccountVM = CreateAccountVM()
  
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      self.title = "Create your account"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


  
  @IBAction func createPressed(_ sender: UIButton) {
    spinner.startAnimating()
    self.view.endEditing(true)
    let message =  userValid(name: nameText.text!, email: emailText.text!, password: passwordText.text!)
    guard message.isEmpty else {
      let alert = UIAlertController(title: "Ops", message: message, preferredStyle: .alert)
      let cancelAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
      alert.addAction(cancelAction)
      self.present(alert, animated: true, completion: nil)
      return
    }
    createAccountVM.name     = nameText.text!
    createAccountVM.password = passwordText.text!
    createAccountVM.email    = emailText.text!

    let _ = createAccountVM.rxCreate()
      .observeOn(MainScheduler.instance)
      .subscribe{result in
        let status = result.element
        guard status == 200 else {
          self.spinner.stopAnimating()
          self.showAlert(message: self.createAccountVM.errorMessage)
          return
        }
        self.spinner.stopAnimating()
    }
  }
}

extension CreateAccountViewController: UITextFieldDelegate{
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    self.view.endEditing(true)
    return true
  }
}
