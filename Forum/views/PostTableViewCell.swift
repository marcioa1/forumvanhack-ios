//
//  PostTableViewCell.swift
//  Forum
//
//  Created by Admin on 29/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
  
  @IBOutlet weak var commentLabel: UILabel!
  @IBOutlet weak var userLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func setup(_ post: Post) {
    commentLabel.text  = post.comment
    userLabel.text     = post.user 
  }
  
}
