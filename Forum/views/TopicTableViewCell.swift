//
//  TopicTableViewCell.swift
//  Forum
//
//  Created by Admin on 28/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import UIKit

class TopicTableViewCell: UITableViewCell {
  
  @IBOutlet weak var contentLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var postCounterLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  func setup(_ topic:Topic){
    titleLabel.text   = topic.title
    contentLabel.text = topic.content
    postCounterLabel.text = String(topic.posts.count)
    postCounterLabel?.layer.cornerRadius = 10.0
    postCounterLabel?.layer.masksToBounds = true
    self.selectionStyle = .none
  }
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}
