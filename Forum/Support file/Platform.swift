//
//  Platform.swift
//  Presenteia
//
//  Created by Marcio Aun Migueis on 7/6/16.
//  Copyright © 2016 AR1. All rights reserved.
//

import Foundation

struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}
