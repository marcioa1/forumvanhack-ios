//
//  Url.swift
//  RobertosClock
//
//  Created by Admin on 02/09/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import Foundation

struct Url {
  
  static var LOCAL_LOGIN  = "http://localhost:3000/api/users/login"
  static var SERVER_LOGIN = "https://forum-vanhack.herokuapp.com/api/users/login"
  
  
  static var LOCAL_CREATE_ACCOUNT  = "http://localhost:3000/api/users"
  static var SERVER_CREATE_ACCOUNT = "https://forum-vanhack.herokuapp.com/api/users"

  static var LOCAL_TOPICS = "http://localhost:3000/api/topics"
  static var SERVER_TOPICS = "https://forum-vanhack.herokuapp.com/api/topics"

  static var LOCAL_POSTS = "http://localhost:3000/api/topics/topic_id/posts"
  static var SERVER_POSTS = "https://forum-vanhack.herokuapp.com/api/topics/topic_id/posts"

}
