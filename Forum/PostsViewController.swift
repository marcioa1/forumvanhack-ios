//
//  PostViewController.swift
//  Forum
//
//  Created by Admin on 29/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import UIKit
import RxSwift

class PostsViewController: UIViewController, ViewHelper {
  
  var postVM     = PostVM()
  var topic      = Topic()
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var nameTopicLabel: UILabel!
  @IBOutlet weak var contentTopicText: UITextView!
  @IBOutlet weak var tableview: UITableView!
  @IBOutlet weak var postText: UITextView!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.title = "Posts"
    nameTopicLabel.text   = topic.title
    contentTopicText.text = topic.content
    tableview.tableFooterView = UIView()
    tableview.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "postCell")
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func savePressed(_ sender: UIButton) {
    guard !postText.text.isEmpty else {
      showAlert(message: "Your comment can't be blank")
      return
    }
    spinner.startAnimating()
    postVM.post = Post(comment: postText.text!, user: "", date: Date(), topic_id: topic.id)
    self.view.endEditing(true)
    let _ = postVM.rxCreate()
      .observeOn(MainScheduler.instance)
      .subscribe{result in
        let status = result.element
        guard status == 200 else {
          self.spinner.stopAnimating()
          self.showAlert(message: self.postVM.errorMessage)
          return
        }
        self.spinner.stopAnimating()
        _ = self.navigationController?.popViewController(animated: true)
    }
    .disposed(by: disposeBag)
  }
}

extension PostsViewController: UITableViewDataSource{
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return topic.posts.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell  = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostTableViewCell
    let post = topic.posts[indexPath.row]
    cell.setup(post)
    return cell
  }
}

extension PostsViewController: UITableViewDelegate{
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 97.0
  }
}
