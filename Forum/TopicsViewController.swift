//
//  TopicsViewController.swift
//  Forum
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 br.com.ar1mobile. All rights reserved.
//

import UIKit
import RxSwift

class TopicsViewController: UIViewController, ViewHelper {
  
  var topicVM       = TopicVM()
  var disposeBag    = DisposeBag()
  var selectedTopic = Topic()
  var topics        = [Topic]()
  var selectedIndex = 0
  
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  @IBOutlet weak var tableview: UITableView!
  
  lazy var refreshControl: UIRefreshControl = {
    let refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: #selector(getDataFromServer), for: UIControlEvents.valueChanged)
    
    return refreshControl
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.title = "Topics"
    spinner.startAnimating()
    tableview.addSubview(self.refreshControl)
    tableview.register(UINib(nibName: "TopicTableViewCell", bundle: nil), forCellReuseIdentifier: "topicCell")
    tableview.tableFooterView = UIView()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    getDataFromServer()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func getDataFromServer(){
    let _ = topicVM.rxIndex()
      .observeOn(MainScheduler.instance)
      .subscribe{
        status in
        let result = status.element
        guard result != nil else {
          self.spinner.stopAnimating()
          return
        }
        guard result == 200 else {
          self.spinner.stopAnimating()
          self.showAlert(message: "Fail to fetch topics. Try again later please.")
          return
        }
        self.topics = self.topicVM.topics
        self.orderByIndex(index: self.selectedIndex)
        self.tableview.reloadData()
        self.spinner.stopAnimating()
        return
      }
      .disposed(by: disposeBag)
  }
  
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "show posts"{
      let postsVC = segue.destination as! PostsViewController
      postsVC.topic = selectedTopic
    }else {
      
    }
  }
  
  @IBAction func orderSelected(_ sender: UISegmentedControl) {
    selectedIndex = sender.selectedSegmentIndex
    orderByIndex(index: sender.selectedSegmentIndex)
  }
  
  func orderByIndex(index:Int){
    switch index {
    case 0:
      topics = topicVM.topics
    case 1:
      topics = topicVM.topics.sorted(by: { $0.posts.count > $1.posts.count })
    default:
      topics = topicVM.topics
    }
    tableview.reloadData()
    let indexPath = IndexPath(row: 0, section: 0)
    tableview.scrollToRow(at: indexPath, at: .top, animated: true)
  }
}

extension TopicsViewController: UITableViewDataSource{
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.topics.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell  = tableView.dequeueReusableCell(withIdentifier: "topicCell", for: indexPath) as! TopicTableViewCell
    let topic = topics[indexPath.row]
    cell.setup(topic)
    return cell
  }
  
}

extension TopicsViewController: UITableViewDelegate{
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    selectedTopic = topics[indexPath.row]
    performSegue(withIdentifier: "show posts", sender: self)
  }
  
}
